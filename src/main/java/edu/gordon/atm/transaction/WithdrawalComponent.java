package edu.gordon.atm.transaction;

import com.google.common.eventbus.Subscribe;

public class WithdrawalComponent {

	/**
	 * The handleEvent() method is called whenever the event type in the method
	 * parameter is posted on the event bus.
	 * 
	 * @param evt
	 *            The event this method gets triggered by.
	 */
	@Subscribe
	public void handleEvent(WithdrawalEvent evt) {

	}
}
