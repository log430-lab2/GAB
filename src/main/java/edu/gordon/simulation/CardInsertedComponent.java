package edu.gordon.simulation;

import com.google.common.eventbus.Subscribe;

import edu.gordon.simulation.physical.ATM;

public class CardInsertedComponent {
	private ATM atm;

	public CardInsertedComponent(ATM atm) {
		this.atm = atm;
	}

	@Subscribe
	public void handleEvent(CardInsertedEvent e) {
		atm.cardInserted();
	}
}
