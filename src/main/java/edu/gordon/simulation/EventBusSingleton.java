package edu.gordon.simulation;

import com.google.common.eventbus.EventBus;

public class EventBusSingleton {
	private static EventBus bus = new EventBus();

	public static EventBus getInstance() {
		return bus;
	}
}
