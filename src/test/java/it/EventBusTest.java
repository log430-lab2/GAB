package it;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import edu.gordon.simulation.CardInsertedEvent;
import edu.gordon.simulation.EventBusSingleton;
import edu.gordon.simulation.SimCardInsertedComponent;
import edu.gordon.simulation.physical.ATM;
import edu.gordon.simulation.physical.CardReader;

public class EventBusTest {

	EventBus eventBus;
	ATM atmTest;
	CardReader cardReader;
	DeadEventListener deadEventListener;

	@Before
	public void setUp() throws Exception {
		eventBus = new EventBus();
		atmTest = new ATM(1, "fdsfsdf", "bank", null, eventBus);
		cardReader = new CardReader(eventBus);
		deadEventListener = new DeadEventListener();
	}

	@After
	public void tearDown() throws Exception {

	}

	// Publish-Subscribe test
	// http://kaczanowscy.pl/tomek/2014-06/testing-google-eventbus
	@Test
	public void testCardInsertedEvent() {

		EventBusSingleton.getInstance().register(new SimCardInsertedComponent(eventBus));
		EventBusSingleton.getInstance().register(deadEventListener);

		eventBus.post(new CardInsertedEvent());

		System.out.println(deadEventListener.getEvents());
		assertTrue(deadEventListener.getEvents().isEmpty());
	}

	private class DeadEventListener {
		private Set<DeadEvent> events = new HashSet<DeadEvent>();

		@Subscribe
		public void obtenirDeadEvents(DeadEvent event) {
			events.add(event);
		}

		public Set<DeadEvent> getEvents() {
			return events;
		}
	}
}
