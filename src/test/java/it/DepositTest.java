package it;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.eventbus.EventBus;

import edu.gordon.atm.Balances;
import edu.gordon.atm.Card;
import edu.gordon.atm.Message;
import edu.gordon.atm.Money;
import edu.gordon.atm.transaction.Deposit;
import edu.gordon.atm.transaction.Inquiry;
import edu.gordon.atm.transaction.Withdrawal;
import edu.gordon.simulation.SimulatedBank;
import edu.gordon.simulation.Simulation;
import edu.gordon.simulation.physical.ATM;
import edu.gordon.simulation.physical.NetworkToBank;
import edu.gordon.simulation.physical.Session;

public class DepositTest {

	protected ATM atmTest;
	protected Simulation simTest;
	protected Session sessionTest;
	protected Card cardTest;
	protected SimulatedBank bc;
	protected Withdrawal w;
	protected NetworkToBank ntb;
	protected Inquiry receipt;
	protected Message msg;
	protected Money total;
	protected Money argentdisponible;
	protected Balances b;
	protected Deposit t;
	protected Money depo;

	@Before
	public void setUp() throws Exception {
		EventBus eventBus = new EventBus();
		atmTest = new ATM(1, "fdsfsdf", "bank", null, eventBus);
		simTest = new Simulation(atmTest);
		sessionTest = new Session(atmTest);
		cardTest = new Card(1);

		b = new Balances();
		depo = new Money(20);
		total = new Money(120);
		argentdisponible = new Money(100);
		msg = new Message(Message.INITIATE_DEPOSIT, cardTest, 42, 3, -1, 1, depo);
		b.setBalances(total, argentdisponible);
		t = new Deposit(atmTest, sessionTest, cardTest, 42);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void performTransactionTest() {
		// Status status = atmTest.getNetworkToBank().sendMessage(msg, b);
		assertEquals(true, this.atmTest.getNetworkToBank().sendMessage(msg, b).isSuccess());
		//assertEquals(true, this.atmTest.getNetworkToBank().sendMessage(msg, b).isSuccess());
	}

	@Test
	public void testgetBalances() {

		assertEquals(total, b.getTotal());
	}

	@Test
	public void testgetAvailable() {
		// TODO
		assertEquals(argentdisponible, b.getAvailable());
	}

}
