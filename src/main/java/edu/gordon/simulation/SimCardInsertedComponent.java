package edu.gordon.simulation;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class SimCardInsertedComponent {
	private EventBus eventBus;

	public SimCardInsertedComponent(EventBus eventBus){
		this.eventBus = eventBus;
	}
	@Subscribe
	public void handleEvent(SimCardInsertedEvent e) {
		eventBus.post(new CardInsertedEvent());
	}
}
