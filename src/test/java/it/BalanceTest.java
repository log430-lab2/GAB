package it;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.gordon.atm.Balances;
import edu.gordon.atm.Card;
import edu.gordon.atm.Message;
import edu.gordon.atm.Money;
import edu.gordon.atm.transaction.Inquiry;
import edu.gordon.atm.transaction.Withdrawal;
import edu.gordon.simulation.SimulatedBank;
import edu.gordon.simulation.Simulation;
import edu.gordon.simulation.physical.ATM;
import edu.gordon.simulation.physical.CardReader;
import edu.gordon.simulation.physical.NetworkToBank;
import edu.gordon.simulation.physical.Session;

public class BalanceTest {

	protected ATM atmTest;
	protected Simulation simTest;
	protected Session sessionTest;
	protected Card cardTest;
	protected SimulatedBank bc;
	protected Withdrawal w;
	protected NetworkToBank ntb;
	protected Balances balance;
	protected Inquiry receipt;
	protected Message msg;
	protected Money total;
	protected Money argentdisponible;
	protected Balances b;
	protected CardReader cardReader;

	@Before
	public void setUp() throws Exception {

		b = new Balances();
		total = new Money(100);
		argentdisponible = new Money(80);
		b.setBalances(total, argentdisponible);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testgetBalances() {

		assertEquals(total, b.getTotal());
	}

	@Test
	public void testgetAvailable() {
		// TODO
		assertEquals(argentdisponible, b.getAvailable());
	}
}
