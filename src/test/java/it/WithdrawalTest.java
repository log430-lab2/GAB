package it;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.eventbus.EventBus;

import edu.gordon.atm.Balances;
import edu.gordon.atm.Card;
import edu.gordon.atm.Message;
import edu.gordon.atm.Money;
import edu.gordon.atm.transaction.Inquiry;
import edu.gordon.atm.transaction.Withdrawal;
import edu.gordon.simulation.SimulatedBank;
import edu.gordon.simulation.Simulation;
import edu.gordon.simulation.physical.ATM;
import edu.gordon.simulation.physical.NetworkToBank;
import edu.gordon.simulation.physical.Session;

public class WithdrawalTest {

	protected ATM atmTest;
	protected Simulation simTest;
	protected Session sessionTest;
	protected Card cardTest;
	protected SimulatedBank bc;
	protected Withdrawal w;
	protected NetworkToBank ntb;
	protected Inquiry receipt;
	protected Message msg;
	protected Money total;
	protected Money argentdisponible;
	protected Balances b;
	protected Money withdrawAmount;

	@Before
	public void setUp() throws Exception {
		EventBus eventBus = new EventBus();
		atmTest = new ATM(1, "fdsfsdf", "bank", null, eventBus);
		simTest = new Simulation(atmTest);
		sessionTest = new Session(atmTest);
		cardTest = new Card(1);

		b = new Balances();
		withdrawAmount = new Money(40);
		total = new Money(60);
		argentdisponible = new Money(60);
		msg = new Message(Message.WITHDRAWAL, cardTest, 42, 3, 1, -1, withdrawAmount);
		b.setBalances(total, argentdisponible);
		// w = new Withdrawal(atmTest, sessionTest, cardTest, 42);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() { // TODO
		assertEquals(true, this.atmTest.getNetworkToBank().sendMessage(msg, b).isSuccess());
		//handlemessage()
		//handle...
	}


	@Test
	public void testgetBalances() {

		assertEquals(total, b.getTotal());
	}

	@Test
	public void testgetAvailable() {
		// TODO
		assertEquals(argentdisponible, b.getAvailable());
	}


	/*
	 * @Before public void setUp() { balance.setBalances(new Money(100), new
	 * Money(100)); }
	 */

	/*
	 * public void setUp() throws Exception { atmTest = new ATM(42,
	 * "Gordon College", "First National Bank of Podunk", null /* We're not
	 * really talking to a bank! ); simTest = new Simulation(atmTest); ntb = new
	 * NetworkToBank(new Log(), null);
	 * 
	 * // Create the frame that will display the simulated ATM, and add the //
	 * GUI edu.gordon.simulation to it
	 * 
	 * Frame mainFrame = new Frame("ATM Simulation");
	 * mainFrame.add(simTest.getGUI());
	 * 
	 * // Arrange for a file menu with a Quit option, plus quit on window close
	 * 
	 * MenuBar menuBar = new MenuBar(); Menu fileMenu = new Menu("File");
	 * MenuItem quitItem = new MenuItem("Quit", new MenuShortcut('Q'));
	 * quitItem.addActionListener(new ActionListener() { public void
	 * actionPerformed(ActionEvent e) { System.exit(0); } });
	 * fileMenu.add(quitItem); menuBar.add(fileMenu);
	 * mainFrame.setMenuBar(menuBar); mainFrame.addWindowListener(new
	 * WindowAdapter() {
	 * 
	 * @Override public void windowClosing(WindowEvent e) { System.exit(0); }
	 * });
	 * 
	 * // Start the Thread that runs the ATM
	 * 
	 * new Thread(atmTest).start();
	 * 
	 * // Pack the GUI frame, show it, and off we go!
	 * 
	 * mainFrame.setResizable(false); mainFrame.pack();
	 * mainFrame.setVisible(true); }
	 */

}
