package it;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BalanceTest.class, DepositTest.class, TransferTest.class, WithdrawalTest.class, EventBusTest.class })
public class AllTests {

}
